package com.boomzoom.myweather.data.dao

import androidx.room.*
import com.boomzoom.myweather.data.models.here.Town

/********************************************************
 * Created by Andrey Venzhega(BoomZoom) on 09.05.2019|10:07
 ********************************************************/
@Dao
interface TownDao {

    @Query("SELECT * FROM Town")
    fun getAll():List<Town>

    @Query("SELECT  * FROM Town WHERE id = :id LIMIT 1")
    fun getById(id:Long):Town

    @Query("SELECT  * FROM Town WHERE woeid = :woeid LIMIT 1")
    fun getByWoeid(woeid:Int):Town

    @Insert
    fun insert(town: Town)

    @Update
    fun update(town: Town)

    @Delete
    fun delate(town: Town)
}