package com.boomzoom.myweather.data.models.network

import com.google.gson.annotations.SerializedName


data class LocationSearch(
    val distance: Int,
    @SerializedName("latt_long")
    val lattLong: String,
    @SerializedName("location_type")
    val locationType: String,
    val title: String,
    val woeid: Int
)