package com.boomzoom.myweather.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.boomzoom.myweather.data.dao.TownDao
import com.boomzoom.myweather.data.models.here.Town

/********************************************************
 * Created by Andrey Venzhega(BoomZoom) on 09.05.2019|10:17
 ********************************************************/
@Database(entities = [Town::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun townDao(): TownDao

}