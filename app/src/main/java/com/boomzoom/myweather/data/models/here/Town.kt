package com.boomzoom.myweather.data.models.here

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

/********************************************************
 * Created by Andrey Venzhega(BoomZoom) on 09.05.2019|09:36
 ********************************************************/
@Entity(indices = [Index(value = ["woeid"], unique = true)])
data class Town(
    val title: String,
    @ColumnInfo(name = "location_type")
    val locationType: String,
    @ColumnInfo(name = "woeid")
    val woeid: Int,
    @ColumnInfo(name = "latt_long")
    val lattLong: String
){
    @PrimaryKey(autoGenerate = true)
    var id: Long=0
}