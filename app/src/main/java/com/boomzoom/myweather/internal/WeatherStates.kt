package com.boomzoom.myweather.internal

/********************************************************
 * Created by Andrey Venzhega(BoomZoom) on 01.05.2019|21:01
 ********************************************************/
enum class WeatherStates {
    Snow {
        override fun toString(): String = "sn"
    },
    Sleet {
        override fun toString(): String = "sl"
    },
    Hail {
        override fun toString(): String = "h"
    },
    Thunderstorm {
        override fun toString(): String = "t"
    },
    HeavyRain {
        override fun toString(): String = "hr"
    },
    LightRain {
        override fun toString(): String = "lr"
    },
    Showers {
        override fun toString(): String = "s"
    },
    HeavyCloud {
        override fun toString(): String = "hc"
    },
    LightCloud {
        override fun toString(): String = "lc"
    },
    Clear {
        override fun toString(): String = "c"
    }
}