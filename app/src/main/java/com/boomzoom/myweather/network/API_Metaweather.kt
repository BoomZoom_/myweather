package com.boomzoom.myweather.network

import android.annotation.SuppressLint
import com.boomzoom.myweather.data.models.network.LocationSearch
import com.boomzoom.myweather.data.models.network.WeatherInPlace.WeatherInPlace
import com.boomzoom.myweather.data.models.network.locationWeatherForDate
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.Call
import java.time.LocalDate
import java.util.*


/********************************************************
 * Created by Andrey Venzhega(BoomZoom) on 01.05.2019|14:49
 ********************************************************/
interface API_Metaweather {

    @GET("location/search/?lattlong={latt},{long}")
    fun locationSearchRequest(
        @Path("latt") latt: String,
        @Path("long") long: String
    ): Call<List<LocationSearch>>

    @GET("location/search/")
    fun locationSearchRequest(@Query("query") query: String): Call<List<LocationSearch>>

    @GET("location/{woeid}/")
    fun weatherInPlaceRequest(@Path("woeid") woeid: Int): Call<WeatherInPlace>

    //Coroutines
    @GET("location/search/")
    fun locationSearchRequestCoroutines(@Query("query") query: String): Deferred<List<LocationSearch>>

    @GET("location/{woeid}/")
    fun weatherInPlaceRequestCoroutines(@Path("woeid") woeid: Int): Deferred<WeatherInPlace>

    @GET("location/{woeid}/{year}/{month}/{day}/")
    fun locationWeatherForDateRequestCoroutines(
        @Path("woeid") woeid: Int,
        @Path("year") year: Int,
        @Path("month") month: Int,
        @Path("day") day: Int
    )
            : Deferred<List<locationWeatherForDate>>


}