package com.boomzoom.myweather.network

import android.content.Context
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import mekotlinapps.dnyaneshwar.`in`.restdemo.util.hasNetwork
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/********************************************************
 * Created by Andrey Venzhega(BoomZoom) on 01.05.2019|14:46
 ********************************************************/


object RetrofitInit {
    private var weathre: API_Metaweather? = null;
    fun metaweather(context: Context): API_Metaweather {
        if (weathre != null) return weathre!!

        val cacheSize = (5 * 1024 * 1024).toLong()
        val myCache = Cache(context.cacheDir, cacheSize)

        val okHttpClient = OkHttpClient.Builder()
            .cache(myCache)
            .addInterceptor { chain ->
                var request = chain.request()
                request = if (hasNetwork(context))
                    request.newBuilder().header("Cache-Control", "public, max-age=" + 3600).build()
                else
                    request.newBuilder().header(
                        "Cache-Control",
                        "public, only-if-cached, max-stale=${60 * 60 * 24 * 7},min-fresh=3600"
                    ).build()
                chain.proceed(request)
            }
            .build()


        val retrofit = Retrofit.Builder()
            .baseUrl("https://www.metaweather.com/api/")
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()

        weathre = retrofit.create(API_Metaweather::class.java)
        return weathre!!
    }

}