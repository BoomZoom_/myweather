package com.boomzoom.myweather.util

import android.content.Context
import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.util.Log
import android.widget.ImageView
import com.boomzoom.myweather.data.models.network.WeatherInPlace.WeatherInPlace
import com.boomzoom.myweather.internal.WeatherStates
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder

/********************************************************
 * Created by Andrey Venzhega(BoomZoom) on 09.05.2019|23:25
 ********************************************************/

fun fillWeatherImg(weatherStates: WeatherStates, imageView: ImageView,context: Context) {
    val buffer= mutableMapOf<WeatherStates,RequestBuilder<Drawable>>()

    if(buffer[weatherStates]==null)
        buffer[weatherStates] = Glide.with(context)
        .load("https://www.metaweather.com/static/img/weather/png/64/$weatherStates.png")

    buffer[weatherStates]?.into(imageView)
}

fun abbreviationInEnum(weatherInPlace: WeatherInPlace): WeatherStates {
    return when (weatherInPlace.consolidatedWeather[0].weatherStateAbbr) {
        "sn" -> WeatherStates.Snow
        "sl" -> WeatherStates.Sleet
        "h" -> WeatherStates.Hail
        "t" -> WeatherStates.Thunderstorm
        "hr" -> WeatherStates.HeavyRain
        "lr" -> WeatherStates.LightRain
        "s" -> WeatherStates.Showers
        "hc" -> WeatherStates.HeavyCloud
        "lc" -> WeatherStates.LightCloud
        "c" -> WeatherStates.Clear
        else -> throw Resources.NotFoundException("abbreviation not found!");
    }
}

fun abbreviationInEnum(abbr: String): WeatherStates {
    return when (abbr) {
        "sn" -> WeatherStates.Snow
        "sl" -> WeatherStates.Sleet
        "h" -> WeatherStates.Hail
        "t" -> WeatherStates.Thunderstorm
        "hr" -> WeatherStates.HeavyRain
        "lr" -> WeatherStates.LightRain
        "s" -> WeatherStates.Showers
        "hc" -> WeatherStates.HeavyCloud
        "lc" -> WeatherStates.LightCloud
        "c" -> WeatherStates.Clear
        else -> throw Resources.NotFoundException("abbreviation not found!");
    }
}