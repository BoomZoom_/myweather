package com.boomzoom.myweather

import android.app.Application
import com.boomzoom.myweather.data.AppDatabase
import androidx.room.Room


/********************************************************
 * Created by Andrey Venzhega(BoomZoom) on 09.05.2019|10:33
 ********************************************************/
class App: Application() {
    lateinit var database: AppDatabase
    override fun onCreate() {
        super.onCreate()
        instance=this
        database = Room.databaseBuilder(this, AppDatabase::class.java, "database")
            .build()
    }

    companion object{
        lateinit var instance:App
    }
}