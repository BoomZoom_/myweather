package com.boomzoom.myweather.ui.fragment.bookmarks

import android.content.Context
import android.content.SharedPreferences
import android.database.sqlite.SQLiteConstraintException
import android.database.sqlite.SQLiteException
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.boomzoom.myweather.App
import com.boomzoom.myweather.R
import com.boomzoom.myweather.data.AppDatabase
import com.boomzoom.myweather.data.models.here.Town
import com.boomzoom.myweather.data.models.network.LocationSearch
import kotlinx.android.synthetic.main.bookmarks_fragment.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jetbrains.anko.support.v4.toast
import java.lang.Exception


class BookmarksFragment : Fragment() {

    private lateinit var appDatabase: AppDatabase

    private lateinit var viewModel: BookmarksViewModel

    private lateinit var adapter: BookmarksAdapter

    private lateinit var preferences: SharedPreferences

    companion object {
        fun newInstance() = BookmarksFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.bookmarks_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(BookmarksViewModel::class.java)


        recyclerViewBookmarks.layoutManager = LinearLayoutManager(activity)

        appDatabase = App.instance.database
        val townDao = appDatabase.townDao()


        GlobalScope.launch {
            val towns: List<Town> = townDao.getAll()
            GlobalScope.launch(Dispatchers.Main) {
                adapter = BookmarksAdapter(towns)
                recyclerViewBookmarks.adapter = adapter
            }
        }

        preferences = activity?.getSharedPreferences(
            getString(R.string.preference_file_key), Context.MODE_PRIVATE
        )!!
        (activity as AppCompatActivity).supportActionBar?.title = "Bookmarks"
        (activity as AppCompatActivity).supportActionBar?.subtitle =null
    }

    override fun onContextItemSelected(item: MenuItem?): Boolean {


        return when (item?.itemId) {
            R.id.showTheWeather -> {
                val editor: SharedPreferences.Editor = preferences.edit()
                editor.putInt(getString(R.string.townWoeid), adapter.woeid)
                editor.apply()
                true
            }
            R.id.deleteBookmarks -> {
                if (adapter.itemCount == 1) {
                    toast("impossible to delete a single city")
                    true
                } else {
                    GlobalScope.launch {
                        try {
                            val townDao = appDatabase.townDao()
                            val townHere = townDao.getByWoeid(adapter.woeid)
                            townDao.delate(townHere)

                            adapter = BookmarksAdapter(townDao.getAll())

                            GlobalScope.launch(Dispatchers.Main) {
                                toast("i here")
                                recyclerViewBookmarks.adapter = adapter
                                adapter.notifyDataSetChanged()
                            }
                        } catch (e: SQLiteException) {
                            Log.e("sql", e.message.toString())
                        } catch (e: java.lang.NullPointerException) {
                            GlobalScope.launch(Dispatchers.Main) {
                                toast("this town not found")
                            }
                            Log.e("sql", "this town not found")
                        } catch (e: Exception) {
                            Log.e("sql", "oops")
                        }
                    }
                    true
                }
            }
            else -> super.onContextItemSelected(item)
        }
    }
}
