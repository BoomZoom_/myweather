package com.boomzoom.myweather.ui.fragment.home

//import kotlinx.android.synthetic.main.activity_main.*
import android.content.Context
import android.content.res.Resources
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.boomzoom.myweather.R
import com.boomzoom.myweather.data.models.network.WeatherInPlace.WeatherInPlace
import com.boomzoom.myweather.internal.WeatherStates
import com.boomzoom.myweather.network.API_Metaweather
import com.boomzoom.myweather.network.RetrofitInit
//import com.boomzoom.myweather.network.apiWeather
import com.boomzoom.myweather.ui.activity.MainActivity
import com.boomzoom.myweather.util.abbreviationInEnum
import com.boomzoom.myweather.util.fillWeatherImg
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.home_fragment.*
import kotlinx.coroutines.*
import org.jetbrains.anko.support.v4.toast
import retrofit2.*
import java.lang.Exception

class HomeFragment : Fragment() {
    private val TAG: String = HomeFragment::class.java.simpleName

    private lateinit var main: MainActivity
    private lateinit var job: Job
    private var woeid: Int=-1

    companion object {
        fun newInstance() = HomeFragment()
    }

    private lateinit var viewModel: HomeViewModel
    private lateinit var apiWeather: API_Metaweather

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        main = parentFragment!!.activity as MainActivity
        return inflater.inflate(R.layout.home_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)

        apiWeather = RetrofitInit.metaweather(context = activity!!)

        val preferences = activity?.getSharedPreferences(
            getString(R.string.preference_file_key),
            Context.MODE_PRIVATE
        )!!

        woeid = preferences.getInt(getString(R.string.townWoeid), 924938)
        weatherInPlaceCoroutines(woeid)

    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }

    @Deprecated("use weatherInPlaceCoroutines")
    private fun weatherInPlace(woeid: Int) {
        val call = apiWeather.weatherInPlaceRequest(woeid = woeid)

        call.enqueue(object : Callback<WeatherInPlace> {
            override fun onResponse(call: Call<WeatherInPlace>, response: Response<WeatherInPlace>) {
                val response = response.body()!!
                fillWeather(response)
                changeBackground(response)
                progressBar.visibility = View.GONE
            }

            override fun onFailure(call: Call<WeatherInPlace>, t: Throwable) {
                Log.e(TAG, t.toString())
                progressBar.visibility = View.GONE
            }
        })
    }

    private fun weatherInPlaceCoroutines(woeid: Int) {
        progressBar.visibility = View.VISIBLE
        job = GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = apiWeather.weatherInPlaceRequestCoroutines(woeid).await()
                fillWeatherImg(abbreviationInEnum(response),imageViewIconWeather,activity!!)
                fillWeather(response)
                changeBackground(response)
                progressBar.visibility = View.GONE
                (activity as AppCompatActivity).supportActionBar?.title = response.title
                (activity as AppCompatActivity).supportActionBar?.subtitle ="today"
            } catch (e: HttpException) {
                toast("Exception ${e.message}")
            } catch (e: Exception) {
//               toast("oops!")
            }
        }
    }

    private fun fillWeather(weatherInPlace: WeatherInPlace) {
        val firstItem = weatherInPlace.consolidatedWeather[0];
        textViewDate.text = firstItem.applicableDate
        textViewDayOrNightTemperature.text = "Day: ${firstItem.maxTemp.toInt()} Night: ${firstItem.minTemp.toInt()}"
        textViewMainTemperature.text = "${firstItem.theTemp.toInt()}°C"
        textViewHumidity.text = "Humidity: ${firstItem.humidity}"
        textViewTitleWeather.text = firstItem.weatherStateName

    }

    fun changeBackground(weatherInPlace: WeatherInPlace) {
        main.animationBackground(abbreviationInEnum(weatherInPlace))
    }
}
