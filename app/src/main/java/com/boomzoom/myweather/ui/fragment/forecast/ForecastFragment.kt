package com.boomzoom.myweather.ui.fragment.forecast

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AbsListView
import android.widget.EdgeEffect
import android.widget.ProgressBar
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.boomzoom.myweather.R
import com.boomzoom.myweather.data.models.network.locationWeatherForDate
import com.boomzoom.myweather.network.API_Metaweather
import com.boomzoom.myweather.network.RetrofitInit
import com.boomzoom.myweather.ui.activity.MainActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.forecast_fragment.*
import kotlinx.android.synthetic.main.forecast_item.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.jetbrains.anko.support.v4.toast
import retrofit2.HttpException
import java.lang.Exception
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.Period

import java.util.*


class ForecastFragment : Fragment() {

    private lateinit var viewModel: ForecastViewModel

    private lateinit var adapter: ForecastAdapter

    private lateinit var manager: LinearLayoutManager

    private lateinit var list: MutableList<locationWeatherForDate>

    private lateinit var apiWeather: API_Metaweather

    private lateinit var calendar: GregorianCalendar

    private lateinit var progressBar: ProgressBar

    private var currentItems: Int = 0
    private var totalItems: Int = 0
    private var scrollOutItems: Int = 0

    private var woeid: Int = -1

    private var isScrolling = false

    private var job: Job? = null


    companion object {
        fun newInstance() = ForecastFragment()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        progressBar = activity!!.findViewById(R.id.progressBar)
        return inflater.inflate(R.layout.forecast_fragment, container, false)
    }

    @SuppressLint("NewApi")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ForecastViewModel::class.java)
        init()

        manager = LinearLayoutManager(activity)
        recyclerViewForecast.layoutManager = manager
        recyclerViewForecast.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItems = manager.childCount
                totalItems = manager.itemCount
                scrollOutItems = manager.findFirstVisibleItemPosition()

                if (isScrolling && (currentItems + scrollOutItems == totalItems)) {
                    isScrolling = false
                    fetchData()
                }
            }
        })

        firstRequest()
    }

    override fun onDestroy() {
        super.onDestroy()
        job?.cancel()
    }

    private suspend fun downloadForecast() {
        try {
            newPreviousDate()
            val response = apiWeather.locationWeatherForDateRequestCoroutines(
                woeid,
                calendar.time.year + 1900, calendar.time.month, calendar.time.date
            ).await()
            val firstItem = response[0]
            list.add(firstItem)

        } catch (e: HttpException) {
        } catch (e: Exception) {
        }
    }

    private suspend fun downloadForecast(
        range: Int = 10,
        sideEffect: (percent: Int) -> Unit
    ) {
        for (i in 0..range) {
            downloadForecast()
            GlobalScope.launch(Dispatchers.Main) {
                sideEffect.invoke(i * 100 / range)
            }
        }
    }

    private fun waveLoadingUpdate(percent: Int){
        waveLoadingView.progressValue = percent
        waveLoadingView.centerTitle = "$percent %"

        if (percent==100){
            waveLoadingView.visibility = View.GONE
            waveLoadingView.progressValue = 0
            waveLoadingView.centerTitle = "0 %"
        } else if ( percent == 0){
            waveLoadingView.visibility = View.VISIBLE
        }

    }

    private fun init() {
        apiWeather = RetrofitInit.metaweather(context = activity!!)
        (activity as AppCompatActivity).supportActionBar?.title = "Forecast"
        (activity as AppCompatActivity).supportActionBar?.subtitle = null

        val preferences = activity?.getSharedPreferences(
            getString(R.string.preference_file_key),
            Context.MODE_PRIVATE
        )!!

        woeid = preferences.getInt(getString(R.string.townWoeid), 924938)

        calendar = GregorianCalendar()
        calendar.add(Calendar.MONTH, 1)
        calendar.add(Calendar.DAY_OF_MONTH, 5)

    }

    private fun newPreviousDate() {
        calendar.add(Calendar.DAY_OF_MONTH, -1)
    }

    private fun firstRequest() {
        try {
            list = mutableListOf<locationWeatherForDate>()
            job = GlobalScope.launch(Dispatchers.Main) {
                downloadForecast(10, ::waveLoadingUpdate)

                adapter = ForecastAdapter(list)
                recyclerViewForecast?.adapter = adapter
            }
        } catch (e: Exception) {

        }
    }

    private fun fetchData() {
        try {
            job = GlobalScope.launch(Dispatchers.Main) {
                downloadForecast(10,::waveLoadingUpdate)
                adapter.notifyDataSetChanged()
            }
        } catch (e: Exception) {

        }
    }

}
