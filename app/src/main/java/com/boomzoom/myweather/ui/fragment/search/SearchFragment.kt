package com.boomzoom.myweather.ui.fragment.search

//import android.widget.SearchView
import android.content.Context
import android.content.SharedPreferences
import android.database.sqlite.SQLiteConstraintException
import android.database.sqlite.SQLiteException
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.view.MenuItemCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.boomzoom.myweather.App
import com.boomzoom.myweather.R
import com.boomzoom.myweather.data.AppDatabase
import com.boomzoom.myweather.data.models.here.Town
import com.boomzoom.myweather.data.models.network.LocationSearch
import com.boomzoom.myweather.network.RetrofitInit
import com.boomzoom.myweather.ui.activity.MainActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.search_fragment.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jetbrains.anko.support.v4.toast
import retrofit2.HttpException
import java.lang.Exception
import java.lang.NullPointerException


class SearchFragment : Fragment() {

    private lateinit var locationSearches: List<LocationSearch>

    private lateinit var adapter: SearchAdapter

    private lateinit var appDatabase: AppDatabase

    private lateinit var viewModel: SearchViewModel


    companion object {
        fun newInstance() = SearchFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.search_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(SearchViewModel::class.java)

        setHasOptionsMenu(true);

        recyclerViewSearch.layoutManager = LinearLayoutManager(activity)
        locationSearchRequestCoroutines(" ")

        appDatabase = App.instance.database

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
        inflater.inflate(R.menu.menu, menu)

        val menuItem: MenuItem = menu.findItem(R.id.search)
        val searchView = SearchView((activity as MainActivity).supportActionBar!!.themedContext)
//            MenuItemCompat.setShowAsAction(
//                menuItem,
//                MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW or MenuItemCompat.SHOW_AS_ACTION_IF_ROOM
//            )
        MenuItemCompat.setActionView(menuItem, searchView)

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                locationSearchRequestCoroutines(newText?.trim() ?: " ")
                return true
            }
        })

        (activity as AppCompatActivity).supportActionBar?.title = "Search"
        (activity as AppCompatActivity).supportActionBar?.subtitle = null
    }

    override fun onContextItemSelected(item: MenuItem?): Boolean {
        val townHere: LocationSearch = locationSearches[adapter.position]
        val townDao = appDatabase.townDao()
        val townDb: Town = Town(
            townHere.title,
            townHere.locationType,
            townHere.woeid,
            townHere.lattLong
        )
        return when (item?.itemId) {
            R.id.addBookmarks -> {
                GlobalScope.launch {
                    try {
                        townDao.insert(townDb)
                    } catch (e: SQLiteConstraintException) {
                        GlobalScope.launch(Dispatchers.Main) {
                            toast("this city is already there")
                        }
                        Log.e("sql", e.message.toString())

                    } catch (e: Exception) {
                        Log.e("sql", e.message.toString())
                    }
                }
                true
            }
            R.id.deleteBookmarks -> {
                GlobalScope.launch {
                    try {
                        val town = townDao.getByWoeid(townHere.woeid)
                        townDao.delate(town)
                    } catch (e: SQLiteException) {
                        Log.e("sql", e.message.toString())
                    } catch (e: java.lang.NullPointerException) {
                        GlobalScope.launch(Dispatchers.Main) {
                            toast("this town not found")
                        }
                        Log.e("sql", "this town not found")
                    } catch (e: Exception) {
                        Log.e("sql", "oops")
                    }
                }
                true
            }
            else -> super.onContextItemSelected(item)
        }
    }

    private fun locationSearchRequestCoroutines(searchRequest: String) {
        val api = RetrofitInit.metaweather(context = activity!!)
        GlobalScope.launch(Dispatchers.Main) {
            try {
                locationSearches = api.locationSearchRequestCoroutines(searchRequest).await()
                adapter = SearchAdapter(locationSearches)
                recyclerViewSearch.adapter = adapter
                adapter.notifyDataSetChanged()
            } catch (e: HttpException) {
            } catch (e: Exception) {
            }
        }
    }

}
