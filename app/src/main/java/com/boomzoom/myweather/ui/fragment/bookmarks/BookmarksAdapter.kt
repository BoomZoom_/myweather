package com.boomzoom.myweather.ui.fragment.bookmarks

import android.view.*
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.boomzoom.myweather.R
import com.boomzoom.myweather.data.models.here.Town


/********************************************************
 * Created by Andrey Venzhega(BoomZoom) on 08.05.2019|13:40
 ********************************************************/
class BookmarksAdapter(private val towns: List<Town>) : RecyclerView.Adapter<BookmarksAdapter.ViewHolder>() {

    var position = -1
    var woeid=-1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.bookmarks_item,
            parent,
            false
        )
        return ViewHolder(itemView)
    }

    override fun getItemCount() = towns.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val town = towns[position]
        holder.townTitle.text = town.title
        holder.townDetail.text = "${town.locationType}: ${town.lattLong}"

        holder.itemView.setOnLongClickListener {
            this@BookmarksAdapter.position = position
            this@BookmarksAdapter.woeid=town.woeid
            false
        }
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),View.OnCreateContextMenuListener {
        val townTitle: TextView = itemView.findViewById(R.id.townTitle)
        val townDetail: TextView = itemView.findViewById(R.id.townDetail)

        init {
            itemView.setOnCreateContextMenuListener(this)
        }

        override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
            menu!!.add(
                Menu.NONE, R.id.showTheWeather,
                Menu.NONE, R.string.showTheWeather
            )
            menu.add(
                Menu.NONE, R.id.deleteBookmarks,
                Menu.NONE, R.string.deleteBookmarks
            )
        }
    }
}