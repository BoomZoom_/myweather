package com.boomzoom.myweather.ui.activity

import android.app.SearchManager
import android.content.Context
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.constraintlayout.widget.ConstraintLayout

import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.boomzoom.myweather.R
import com.boomzoom.myweather.internal.WeatherStates
import com.boomzoom.myweather.ui.fragment.search.SearchFragment
//import com.boomzoom.myweather.network.apiWeather
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.searchManager

class MainActivity : AppCompatActivity() {

    val TAG: String = MainActivity::class.java.simpleName

    private lateinit var navController: NavController


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        animationBackground(WeatherStates.Clear)

        navController = Navigation.findNavController(this, R.id.nav_host_fragment)

        bottom_nav.setupWithNavController(navController)

        NavigationUI.setupActionBarWithNavController(this, navController)

    }

    override fun onSupportNavigateUp(): Boolean {
        return NavigationUI.navigateUp(null, navController)//(navController, null)
    }

     fun animationBackground(states: WeatherStates) {
        val constraintLayout: ConstraintLayout = findViewById(R.id.container)
        when (states) {
            WeatherStates.Snow,
            WeatherStates.Sleet,
            WeatherStates.Hail,
            WeatherStates.Thunderstorm,
            WeatherStates.HeavyRain -> constraintLayout.setBackgroundResource(R.drawable.gradient_background_coldly)
            WeatherStates.LightRain,
            WeatherStates.Showers,
            WeatherStates.HeavyCloud -> constraintLayout.setBackgroundResource(R.drawable.gradient_background_moderately)
            WeatherStates.LightCloud,
            WeatherStates.Clear -> constraintLayout.setBackgroundResource(R.drawable.gradient_background_hot)
        }

        val animationDrawable: AnimationDrawable = constraintLayout.background as AnimationDrawable
        animationDrawable.setEnterFadeDuration(2000)
        animationDrawable.setExitFadeDuration(4000)
        animationDrawable.start()
    }




}

