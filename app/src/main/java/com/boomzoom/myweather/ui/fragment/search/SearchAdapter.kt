package com.boomzoom.myweather.ui.fragment.search

import android.view.*
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.boomzoom.myweather.R
import com.boomzoom.myweather.data.models.network.LocationSearch


/********************************************************
 * Created by Andrey Venzhega(BoomZoom) on 05.05.2019|10:52
 ********************************************************/
class SearchAdapter(
    private val towns: List<LocationSearch>
) : RecyclerView.Adapter<SearchAdapter.ViewHolder>() {

    var position = -1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(
            R.layout.search_item,
            parent,
            false
        )
        return ViewHolder(view)
    }

    override fun getItemCount() = towns.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val town = this.towns[position]
        holder.townTitle.text = town.title
        holder.townDetail.text = "${town.locationType}: ${town.lattLong}"

        holder.itemView.setOnLongClickListener {
            this@SearchAdapter.position = position
            false
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnCreateContextMenuListener {

        val townTitle: TextView = itemView.findViewById(R.id.townTitle)
        val townDetail: TextView = itemView.findViewById(R.id.townDetail)

        init {
            itemView.setOnCreateContextMenuListener(this)
        }

        override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
            menu!!.add(
                Menu.NONE, R.id.addBookmarks,
                Menu.NONE, R.string.addBookmarks
            )
            menu.add(
                Menu.NONE, R.id.deleteBookmarks,
                Menu.NONE, R.string.deleteBookmarks
            )
            //context.menuInflater.inflate(R.menu.search_bookmarks_menu, menu);
        }


    }
}