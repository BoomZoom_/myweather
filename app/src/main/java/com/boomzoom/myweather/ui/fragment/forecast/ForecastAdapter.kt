package com.boomzoom.myweather.ui.fragment.forecast

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.boomzoom.myweather.R
import com.boomzoom.myweather.data.models.network.locationWeatherForDate
import com.boomzoom.myweather.util.abbreviationInEnum
import com.boomzoom.myweather.util.fillWeatherImg
import kotlin.math.roundToInt

/********************************************************
 * Created by Andrey Venzhega(BoomZoom) on 09.05.2019|20:05
 ********************************************************/
class ForecastAdapter(private val forecasts: List<locationWeatherForDate>) :
    RecyclerView.Adapter<ForecastAdapter.ViewHolder>() {
    private lateinit var context: Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(
            R.layout.forecast_item,
            parent,
            false
        )
        context = parent.context
        return ViewHolder(view)
    }

    override fun getItemCount() = forecasts.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val prediction=forecasts[position]
        fillWeatherImg(
            abbreviationInEnum(prediction.weatherStateAbbr),
            holder.imageViewForecastIconWeather,
            context
        )
        holder.forecastTitle.text = prediction.weatherStateName
        holder.forecastDetail.text = "Day: ${prediction.maxTemp.toInt()} Night: ${prediction.minTemp.toInt()}\n" +
                "Humidity: ${prediction.humidity} Date: ${prediction.applicableDate}"
        holder.forecastTemperature.text= "${prediction.theTemp.toInt()}°C"
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val forecastTitle: TextView = itemView.findViewById(R.id.textViewForecastTitle)
        val forecastDetail: TextView = itemView.findViewById(R.id.textViewForecastDetail)
        val imageViewForecastIconWeather: ImageView = itemView.findViewById(R.id.imageViewForecastIconWeather)
        val forecastTemperature: TextView = itemView.findViewById(R.id.textViewForecastTemperature)
    }
}