package com.boomzoom.myweather.ui.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.boomzoom.myweather.R
import kotlinx.android.synthetic.main.activity_first.*
//import mekotlinapps.dnyaneshwar.`in`.restdemo.util.NetWorkConection
import mekotlinapps.dnyaneshwar.`in`.restdemo.util.hasNetwork

class FirstActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_first)

        if (hasNetwork(applicationContext)) {
            val intent = Intent(this@FirstActivity, MainActivity::class.java)
            startActivity(intent)
        }

        button_try.setOnClickListener{
        if (hasNetwork(applicationContext)) {
            val intent = Intent(this@FirstActivity, MainActivity::class.java)
            startActivity(intent)
        } else {
            Toast.makeText(this, "Please turn on your Internet", Toast.LENGTH_LONG).show()
        }
        }
    }
}
